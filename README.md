# Warsaw Revamped Scripting API Definitions

This repository contains the core and generated definitions used by the scripting API.

## Usage

### Requirements

- [Visual Studio Code](https://code.visualstudio.com/)
- [Luau Language Server](https://marketplace.visualstudio.com/items?itemName=JohnnyMorganz.luau-lsp)

### Required Settings

1. Open the Extension Settings: \
   ![](images/lsp.png)
2. Require Style: `alwaysRelative`\
   ![](images/import_style.png).
3. Plugin: `disabled`\
   ![](images/roblox_plugin.png)
4. Directory Aliases: `.` -> `Mod Cache`\
   ![](images/directory_aliases.png)
5. Require: Mode - `relativeToFile`\
   ![](images/require_mode.png)
6. Types: Definition Files - add `wr-core.d.luau`, `wr-gen.d.luau`, `wr-managers.d.luau`. Order
   matters!\
   ![](images/definition_files.png)
7. Types: Roblox: `disabled`\
   ![](images/types.png)

### Recommended Settings

![](images/recommended_1.png) ![](images/recommended_2.png)

### Recommended Plugins

- [StyLua Formatter](https://marketplace.visualstudio.com/items?itemName=JohnnyMorganz.stylua)
    - Enable "Format on Save"